package at.hed.craftwork.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import javax.validation.constraints.NotNull;

public class TaskCreateDto {

    @NotNull
    private final String title;
    private final String description;
    private final DateTime dueDate;
    private final TaskPriority priority;

    @JsonCreator
    public TaskCreateDto(
            @JsonProperty("title") String title,
            @JsonProperty("description") String description,
            @JsonProperty("dueDate") DateTime dueDate,
            @JsonProperty("priority") TaskPriority priority) {
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public DateTime getDueDate() {
        return dueDate;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "TaskCreateDto{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", dueDate=" + dueDate +
                ", priority=" + priority +
                '}';
    }
}
