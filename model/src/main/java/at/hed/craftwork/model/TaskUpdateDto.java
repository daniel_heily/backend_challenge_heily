package at.hed.craftwork.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

public class TaskUpdateDto {

    private final String title;
    private final String description;
    private final DateTime dueDate;
    private final DateTime resolvedAt;
    private final TaskStatus status;
    private final TaskPriority priority;

    @JsonCreator
    public TaskUpdateDto(
            @JsonProperty("title") String title,
            @JsonProperty("description") String description,
            @JsonProperty("dueDate") DateTime dueDate,
            @JsonProperty("resolvedAt") DateTime resolvedAt,
            @JsonProperty("status") TaskStatus status,
            @JsonProperty("priority") TaskPriority priority) {
        this.title = title;
        this.description = description;
        this.dueDate = dueDate;
        this.resolvedAt = resolvedAt;
        this.status = status;
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public DateTime getDueDate() {
        return dueDate;
    }

    public DateTime getResolvedAt() {
        return resolvedAt;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public TaskPriority getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "TaskUpdateDto{" +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", dueDate=" + dueDate +
                ", resolvedAt=" + resolvedAt +
                ", status=" + status +
                ", priority=" + priority +
                '}';
    }
}
