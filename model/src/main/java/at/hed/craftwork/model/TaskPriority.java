package at.hed.craftwork.model;

public enum TaskPriority {

    LOW, MEDIUM, HIGH;
}
