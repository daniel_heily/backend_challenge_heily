package at.hed.craftwork.model;

public enum TaskStatus {

    OPEN, IN_PROGRESS, FINISHED;
}
