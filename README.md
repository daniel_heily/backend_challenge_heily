# craftwork backend challenge

## Functionality

The application simulates a simple Task Management System. Incoming tasks are pushed to a queue for further processing.

## Project structure

The project is structured in several maven modules

- **api**: this module contains the interfaces for the REST API

- **core**: domain entities, repository interfaces and service classes.

- **model**: module for DTOs that are used by the REST API.

- **server**: within the server module the API interfaces are implemented. Therefor the services from the core module are used.

- **server-bootstrap**: bootstrap module for starting the server. 

### REST Endpoints:

- **task/list** GET fetch all tasks orderd by created at timestamp

- **task?uuid=** GET fetch single task with given uuid

- **task/create** POST create task, input is a TaskCreateDto

- **task/update** POST update task, input is a TaskUpdateDto

- **task/delete?uuid=** DELETE delete task with given uuid

### Task Queue

The task queue is implemented as Spring Component. Tasks created form the scheduler 
and REST API are unified to a single queue. The tasks are orderd by the creation timestamp.
Only tasks with status OPEN are in the queue. After polling a task from the queue, its status
is changed to IN_PROGRESS. 

### Periodic Task creation

A scheduler creates new Tasks at fixed interval (15 sec). 

### Task Consumer

The task consumer simulates the software that consumes the tasks und processes them. 
It polls tasks from the task queue at a fixed interval (25 sec)
