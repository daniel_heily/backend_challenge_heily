package at.hed.craftwork.server.taskqueue;

import at.hed.craftwork.core.domain.Task;
import at.hed.craftwork.core.repository.TaskRepository;
import at.hed.craftwork.model.TaskStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

@Component
public class TaskQueue {

    private static final Logger log = LoggerFactory.getLogger(TaskQueue.class);

    private final TaskRepository taskRepository;
    private LinkedList<Task> queue = new LinkedList<>();

    @Autowired
    public TaskQueue(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Load all tasks in status OPEN in the task queue on startup
     */
    @PostConstruct
    public void init() {
        List<Task> allTasks = taskRepository.findByStatusOrderByCreatedAt(TaskStatus.OPEN);
        queue.addAll(allTasks);
        log.info("Number of tasks in queue: {}", queue.size());
    }

    /**
     * Add Task to the queue
     *
     * @param task the task to add
     */
    public void addTask(Task task) {
        queue.add(task);
        log.info("added task {} to queue", task.toString());
    }

    /**
     * poll the next task from the queue
     *
     * @return the polled task if one is present, null otherwise
     */
    public Task poll() {
        Task task = queue.poll();
        log.info("{} polled from queue", task);
        return task;
    }

    /**
     * Remove task with given uuid from the task queue
     *
     * @param uuid the uuid of the task to remove
     */
    public void removeTask(String uuid) {
        boolean removeFromQueueSuccessful = queue.removeIf(task -> task.getUuid().equals(uuid));
        if (removeFromQueueSuccessful) {
            log.info("Removed task with uuid {} from task queue", uuid);
        }
    }

    /**
     * Get number of tasks currently in queue
     *
     * @return the number of tasks currently in the queue
     */
    public int getNumberOfTasksInQueue() {
        return queue.size();
    }

    /**
     * clear the task queue
     */
    public void clearQueue() {
        queue.clear();
    }
}
