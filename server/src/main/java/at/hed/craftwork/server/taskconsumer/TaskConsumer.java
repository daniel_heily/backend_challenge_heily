package at.hed.craftwork.server.taskconsumer;

import at.hed.craftwork.core.domain.Task;
import at.hed.craftwork.core.service.TaskService;
import at.hed.craftwork.model.TaskStatus;
import at.hed.craftwork.server.taskqueue.TaskQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class TaskConsumer {

    private static final Logger log = LoggerFactory.getLogger(TaskConsumer.class);

    private final TaskQueue taskQueue;
    private final TaskService taskService;

    @Autowired
    public TaskConsumer(TaskQueue taskQueue, TaskService taskService) {
        this.taskQueue = taskQueue;
        this.taskService = taskService;
    }

    /**
     * poll available tasks (status open) form task queue with fixed interval.
     * after polling the task from the queue, the status of the task is changed to IN_PROGRESS
     */
    @Scheduled(fixedRateString = "${task.consumption.interval}")
    public void consumeTask() {
        Task task = taskQueue.poll();
        if (task != null) {
            log.info("consumed {}", task.toString());
            task.setStatus(TaskStatus.IN_PROGRESS);
            taskService.updateTask(task);
        }

    }
}
