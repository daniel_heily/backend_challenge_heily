package at.hed.craftwork.server.impl;

import at.hed.craftwork.api.TaskApi;
import at.hed.craftwork.core.domain.Task;
import at.hed.craftwork.core.domain.TaskFactory;
import at.hed.craftwork.core.service.TaskService;
import at.hed.craftwork.model.TaskCreateDto;
import at.hed.craftwork.model.TaskDto;
import at.hed.craftwork.model.TaskStatus;
import at.hed.craftwork.model.TaskUpdateDto;
import at.hed.craftwork.server.taskqueue.TaskQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class TaskApiImpl implements TaskApi {

    private static final Logger log = LoggerFactory.getLogger(TaskApiImpl.class);

    private final TaskService taskService;
    private final TaskQueue taskQueue;

    public TaskApiImpl(TaskService taskService, TaskQueue taskQueue) {
        this.taskService = taskService;
        this.taskQueue = taskQueue;
    }

    @Override
    @ResponseBody
    public List<TaskDto> getTasks() {
        log.info("getTasks called");
        List<Task> tasks = taskService.getTasksOrderdByCreatedAt();
        List<TaskDto> taskDtos = new ArrayList<>();
        tasks.forEach(task -> taskDtos.add(convertTaskToDto(task)));
        return taskDtos;
    }

    @Override
    @ResponseBody
    public TaskDto createTask(@Valid @RequestBody TaskCreateDto taskCreateDto) {
        log.info("create Task with DTO: {}", taskCreateDto);
        Task newTask = TaskFactory.createTask(taskCreateDto.getTitle(), taskCreateDto.getDescription(), taskCreateDto.getDueDate(), taskCreateDto.getPriority());
        Task createdTask = taskService.createTask(newTask);
        taskQueue.addTask(createdTask);
        return convertTaskToDto(createdTask);
    }

    @Override
    @ResponseBody
    public TaskDto getTask(String uuid) {
        log.info("Get task with uuid {}", uuid);
        Task task = taskService.getTask(uuid);
        if (task != null) {
            return convertTaskToDto(task);
        } else {
            log.info("There is no task available for uuid {}", uuid);
            return null;
        }
    }

    @Override
    public void deleteTask(String uuid) {
        log.info("Delete task with uuid {}", uuid);
        taskService.deleteTask(uuid);
        taskQueue.removeTask(uuid);
    }

    @Override
    @ResponseBody
    public TaskDto update(@PathVariable("uuid") String uuid, @RequestBody TaskUpdateDto taskUpdateDto) {
        log.info("Update task with DTO {}", taskUpdateDto);

        Task toUpdate = taskService.getTask(uuid);

        if (taskUpdateDto.getTitle() != null && !taskUpdateDto.getTitle().equals(toUpdate.getTitle())) {
            toUpdate.setTitle(taskUpdateDto.getTitle());
        }
        if (taskUpdateDto.getDescription() != null && !taskUpdateDto.getDescription().equals(toUpdate.getDescription())) {
            toUpdate.setDescription(taskUpdateDto.getDescription());
        }
        if (taskUpdateDto.getDueDate() != null && !taskUpdateDto.getDueDate().equals(toUpdate.getDueDate())) {
            toUpdate.setDueDate(taskUpdateDto.getDueDate());
        }
        if (taskUpdateDto.getPriority() != null && !taskUpdateDto.getPriority().equals(toUpdate.getTaskPriority())) {
            toUpdate.setTaskPriority(taskUpdateDto.getPriority());
        }
        if (taskUpdateDto.getStatus() != null && !taskUpdateDto.getStatus().equals(toUpdate.getStatus())) {
            toUpdate.setStatus(taskUpdateDto.getStatus());
        }
        if (taskUpdateDto.getResolvedAt() != null && !taskUpdateDto.getResolvedAt().equals(toUpdate.getResolvedAt())) {
            toUpdate.setResolvedAt(taskUpdateDto.getResolvedAt());
        }

        Task updatedTask = taskService.updateTask(toUpdate);

        // if task status was changed by update -  remove it from queue if it is not open anymore
        if (!updatedTask.getStatus().equals(TaskStatus.OPEN)) {
            taskQueue.removeTask(updatedTask.getUuid());
        }
        return convertTaskToDto(updatedTask);
    }

    private TaskDto convertTaskToDto(Task task) {
        return new TaskDto(
                task.getUuid(),
                task.getTitle(),
                task.getDescription(),
                task.getDueDate(),
                task.getResolvedAt(),
                task.getStatus(),
                task.getTaskPriority());
    }
}
