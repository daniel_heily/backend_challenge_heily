package at.hed.craftwork.server;

import at.hed.craftwork.core.CoreConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = {CoreConfiguration.class, ServerConfig.class})
public class ServerConfig {
}
