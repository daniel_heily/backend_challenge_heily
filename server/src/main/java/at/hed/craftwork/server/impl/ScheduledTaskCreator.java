package at.hed.craftwork.server.impl;

import at.hed.craftwork.core.domain.Task;
import at.hed.craftwork.core.domain.TaskFactory;
import at.hed.craftwork.core.service.TaskService;
import at.hed.craftwork.model.TaskPriority;
import at.hed.craftwork.server.taskqueue.TaskQueue;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class ScheduledTaskCreator {

    private final TaskService taskService;
    private final TaskQueue taskQueue;

    @Autowired
    public ScheduledTaskCreator(TaskService taskService, TaskQueue taskQueue) {
        this.taskService = taskService;
        this.taskQueue = taskQueue;
    }

    /**
     * create new tasks at fixed interval
     */
    @Scheduled(fixedRateString = "${task.creation.interval}")
    public void createTask() {
        Task task = TaskFactory.createTask("Scheduled Task", "Task created by task scheduler", DateTime.now().plusDays(7), TaskPriority.MEDIUM);
        Task createdTask = taskService.createTask(task);
        taskQueue.addTask(createdTask);
    }
}
