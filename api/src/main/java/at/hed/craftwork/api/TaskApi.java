package at.hed.craftwork.api;

import at.hed.craftwork.model.TaskCreateDto;
import at.hed.craftwork.model.TaskDto;
import at.hed.craftwork.model.TaskUpdateDto;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("task")
public interface TaskApi {

    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    List<TaskDto> getTasks();

    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    TaskDto createTask(@Valid @RequestBody TaskCreateDto taskCreateDto);

    @ResponseBody
    @RequestMapping(value = "", method = RequestMethod.GET)
    TaskDto getTask(@RequestParam("uuid") String uuid);

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    void deleteTask(@RequestParam("uuid") String uuid);

    @ResponseBody
    @RequestMapping(value = "/update/{uuid}", method = RequestMethod.PATCH)
    TaskDto update(@PathVariable("uuid") String uuid, @RequestBody TaskUpdateDto taskUpdateDto);
}
