package at.hed.craftwork.core.domain;

import at.hed.craftwork.model.TaskPriority;
import at.hed.craftwork.model.TaskStatus;
import org.joda.time.DateTime;

public class TaskFactory {

    public static Task createTask(String title, String description, DateTime dueDate, TaskPriority priority) {
        Task task = new Task();
        task.setTitle(title);
        task.setDescription(description);
        task.setDueDate(dueDate);
        task.setStatus(TaskStatus.OPEN);
        task.setTaskPriority(priority);
        return task;
    }
}
