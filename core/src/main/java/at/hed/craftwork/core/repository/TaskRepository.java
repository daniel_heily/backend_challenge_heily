package at.hed.craftwork.core.repository;

import at.hed.craftwork.core.domain.Task;
import at.hed.craftwork.model.TaskStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    Task findByUuid(String uuid);

    List<Task> findAllByOrderByCreatedAt();

    List<Task> findByStatusOrderByCreatedAt(TaskStatus status);
}
