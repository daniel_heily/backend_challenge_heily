package at.hed.craftwork.core.service;

import at.hed.craftwork.core.domain.Task;
import at.hed.craftwork.core.repository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {

    private static final Logger log = LoggerFactory.getLogger(TaskService.class);

    private final TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task createTask(Task task) {
        return taskRepository.save(task);
    }

    public Task getTask(String uuid) {
        return taskRepository.findByUuid(uuid);
    }

    public List<Task> getTasksOrderdByCreatedAt() {
        return taskRepository.findAllByOrderByCreatedAt();
    }

    public void deleteTask(String uuid) {
        Task toDelete = taskRepository.findByUuid(uuid);
        if (toDelete != null) {
            taskRepository.delete(toDelete);
        } else {
            log.info("No task with given uuid {} present. Task can not be deleted", uuid);
        }
    }

    public Task updateTask(Task taskToUpdate) {
        return taskRepository.save(taskToUpdate);
    }

}
