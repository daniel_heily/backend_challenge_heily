#!/usr/bin/env bash

mvn clean install
docker build . -t heilydan/craftwork
docker login
docker push heilydan/craftwork:latest