package at.hed.craftwork;

import at.hed.craftwork.core.domain.Task;
import at.hed.craftwork.core.domain.TaskFactory;
import org.junit.Test;

import javax.validation.ConstraintViolationException;


public class TaskRepositoryTest extends AbstractTest {

    @Test(expected = ConstraintViolationException.class)
    public void createWithInvalidTitle() {
        Task task = TaskFactory.createTask(null, null, null, null);
        entityManager.persist(task);
        entityManager.flush();
    }

}
