package at.hed.craftwork;

import at.hed.craftwork.api.TaskApi;
import at.hed.craftwork.core.repository.TaskRepository;
import at.hed.craftwork.server.taskqueue.TaskQueue;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@SpringBootTest(classes = ServerBootstrap.class)
public abstract class AbstractTest {


    @Autowired
    protected TestEntityManager entityManager;

    @Autowired
    protected TaskRepository taskRepository;

    @Autowired
    protected TaskApi taskApi;

    @Autowired
    protected TaskQueue taskQueue;
}
