package at.hed.craftwork;

import at.hed.craftwork.model.*;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TaskApiTest extends AbstractTest {

    @Before
    public void setup() {
        taskQueue.clearQueue();
    }

    @Test
    public void createAndGetTask() {
        String title = "Test Task";
        String description = "Task description";
        DateTime dueDate = DateTime.now().plusHours(5);
        TaskPriority priority = TaskPriority.MEDIUM;
        TaskCreateDto taskCreateDto = new TaskCreateDto(title, description, dueDate, priority);
        TaskDto taskDto = taskApi.createTask(taskCreateDto);
        TaskDto task = taskApi.getTask(taskDto.getUuid());
        Assert.assertEquals(title, task.getTitle());
        Assert.assertEquals(description, task.getDescription());
        Assert.assertEquals(dueDate, task.getDueDate());
        Assert.assertEquals(priority, task.getPriority());
        Assert.assertEquals(taskQueue.getNumberOfTasksInQueue(), 1);
    }

    @Test
    public void updateTask() {
        String title = "Test Task";
        String description = "Task description";
        DateTime dueDate = DateTime.now().plusHours(5);
        TaskPriority priority = TaskPriority.MEDIUM;
        TaskCreateDto taskCreateDto = new TaskCreateDto(title, description, dueDate, priority);
        TaskDto taskDto = taskApi.createTask(taskCreateDto);

        Assert.assertEquals(taskQueue.getNumberOfTasksInQueue(), 1);

        String updatedTitle = "updated title";
        String updatedDescription = "updated description";
        DateTime updatedDueDate = DateTime.now();
        DateTime updatedResolvedAt = DateTime.now();
        TaskStatus updatedStatus = TaskStatus.FINISHED;
        TaskPriority updatedPriority = TaskPriority.HIGH;

        TaskUpdateDto taskUpdateDto = new TaskUpdateDto(updatedTitle, updatedDescription,
                updatedDueDate, updatedResolvedAt, updatedStatus, updatedPriority);
        TaskDto updatedDto = taskApi.update(taskDto.getUuid(), taskUpdateDto);

        Assert.assertEquals(updatedTitle, updatedDto.getTitle());
        Assert.assertEquals(updatedDescription, updatedDto.getDescription());
        Assert.assertEquals(updatedDueDate, updatedDto.getDueDate());
        Assert.assertEquals(updatedResolvedAt, updatedDto.getResolvedAt());
        Assert.assertEquals(updatedStatus, updatedDto.getStatus());
        Assert.assertEquals(updatedPriority, updatedDto.getPriority());

        Assert.assertEquals(taskQueue.getNumberOfTasksInQueue(), 0);
    }

    @Test
    public void deleteTask() {
        String title = "Test Task";
        String description = "Task description";
        DateTime dueDate = DateTime.now().plusHours(5);
        TaskPriority priority = TaskPriority.MEDIUM;
        TaskCreateDto taskCreateDto = new TaskCreateDto(title, description, dueDate, priority);
        TaskDto taskDto = taskApi.createTask(taskCreateDto);

        Assert.assertEquals(taskQueue.getNumberOfTasksInQueue(), 1);

        Assert.assertEquals(taskApi.getTasks().size(), 1);
        taskApi.deleteTask(taskDto.getUuid());
        Assert.assertEquals(taskApi.getTasks().size(), 0);
    }

}
