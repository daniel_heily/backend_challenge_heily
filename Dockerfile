FROM java:8
ADD ./server-bootstrap/target/server-bootstrap-1.0-SNAPSHOT.jar server-bootstrap.jar
EXPOSE 8090
CMD java -jar -Dspring.profiles.active=docker,prod server-bootstrap.jar
